'use strict';


var mongoose = require('mongoose'),
    Message = mongoose.model('Message');

exports.list = function(req, res) {
    Message.find({}, function(err, message) {
        if (err)
            return res.send(err);
        return res.json(message);
    });
};

exports.create = function(message) {
    var new_message = new Message(message);
    new_message.save(function(err, message) {
        return err || message;
    });
};


exports.read = function(req, res) {
    let user1 = req.params.authorId;
    let user2 = req.params.recipientId;
    Message.find(
        {
            $or:
                [
                    { 'authorId': user1, 'recipientId': user2 },
                    { 'authorId': user2, 'recipientId': user1 }
                ]
        }
    ).exec(function(err, messages) {
            if (err)
                return res.send(err);
            return res.json(messages);
        }
    );
};
