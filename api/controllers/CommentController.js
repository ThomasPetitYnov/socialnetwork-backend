'use strict';


var mongoose = require('mongoose'),
    Comment = mongoose.model('Comment'),
    Post = mongoose.model('Post');

exports.list = function(req, res) {
    Comment.find({}).populate('author').exec(function(err, comment) {
        if (err)
            return res.send(err);
        return res.json(comment);
    });
};

exports.create = function(req, res) {
    var new_comment = new Comment(req.body).populate('author');
    new_comment.save(function(err, comment) {
        if (err)
            return res.send(err);
        Post.findOne({_id: comment.post}).exec(function(err, post) {
            if (err)
                return res.send(err);
            post.comments.push(comment);
            post.save();
            return res.json(post);
        });
    });
};


exports.read = function(req, res) {
    Comment.findOne({ _id: req.params.commentId}).populate('author').exec(function(err, comment) {
        if (err)
            return res.send(err);
        return res.json(comment);
    });
};


exports.update = function(req, res) {
    Comment.findOneAndUpdate({_id: req.params.commentId}, req.body, {new: true}, function(err, comment) {
        if (err)
            return res.send(err);
        return res.json(comment);
    });
};


exports.delete = function(req, res) {
    Comment.remove({_id: req.params.commentId}, function(err, comment) {
        if (err)
            return res.send(err);
        return res.json({ message: 'Comment successfully deleted !' });
    });
};
