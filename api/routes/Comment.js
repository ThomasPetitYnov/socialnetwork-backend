'use strict';
module.exports = function(app) {
    var comment = require('../controllers/CommentController');

    app.route('/comments')
        .get(comment.list)
        .post(comment.create);


    app.route('/comment/:commentId')
        .get(comment.read)
        .put(comment.update)
        .delete(comment.delete);
};