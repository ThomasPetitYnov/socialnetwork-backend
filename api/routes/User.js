'use strict';
module.exports = function(app) {
    var user = require('../controllers/UserController');

    app.route('/users')
        .get(user.list)
        .post(user.create);


    app.route('/users/:userId')
        .get(user.read)
        .put(user.update)
        .delete(user.delete);

    app.route('/users/find/:username')
        .get(user.findOne)
};