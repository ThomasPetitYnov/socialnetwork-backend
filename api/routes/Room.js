'use strict';
module.exports = function(app) {
    var room = require('../controllers/RoomController');

    app.route('/room/:authorId/:recipientId')
        .get(room.read)
};