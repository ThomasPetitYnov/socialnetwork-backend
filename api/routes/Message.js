'use strict';
module.exports = function(app) {
    var message = require('../controllers/MessageController');

    app.route('/messages')
        .get(message.list);

    app.route('/messages/:authorId/:recipientId')
        .get(message.read)
};