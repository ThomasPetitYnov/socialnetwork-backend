'use strict';
module.exports = function(app) {
    var friend = require('../controllers/FriendController');

    app.route('/friends')
        .get(friend.list)
        .post(friend.create);

    app.route('/friends/:authorId/:recipientId')
        .get(friend.read);

    app.route('/friend/:id')
        .delete(friend.delete)
        .put(friend.update);
};