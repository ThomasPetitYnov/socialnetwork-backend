'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var RoomSchema = new Schema({
    authorId: {
        type: String
    },
    recipientId: {
        type: String
    }
});

module.exports = mongoose.model('Room', RoomSchema);