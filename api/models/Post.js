'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var PostSchema = new Schema({
    content: {
        type: String,
        required: 'Post needs content'
    },
    author: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    likes: [],
    comments: [{ type: Schema.Types.ObjectId, ref: 'Comment' }],
    Created_date: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('Post', PostSchema);