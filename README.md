## What you need

You will need 
[nodejs](https://nodejs.org/en/), 
[MongoDB](https://docs.mongodb.com/v3.2/administration/install-community/) and
[npm](https://www.npmjs.com/)
to run the app.


## How to run the app ?

### Step one :  Clone the Git repository

With HTTPS : `https://gitlab.com/ThomasPetitYnov/socialnetwork-backend.git`

With SSH : `git@gitlab.com:ThomasPetitYnov/socialnetwork-backend.git`

### Step two :  Go to the folder

`cd socialnetwork-backend`

### Step three :  Install all packages

`npm install`

### Step four :  Run the server

`npm start` 

### Step five :  Install the front-end part

**Of course, don't process this step if it's already done !**

The back will work but with no front to use it... 
To fix this issue, make the front work with the same process, explained on 
[its own repository](https://gitlab.com/ThomasPetitYnov/socialnetwork-frontend).